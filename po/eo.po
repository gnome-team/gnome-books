# Esperanto translation for gnome-books.
# Copyright (C) 2011 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnome-books package.
# Tiffany ANTOPOLSKI <tiffany.antopolski@gmail.com>, 2011, 2012.
# Daniel PUENTES <blatberk@openmailbox.org>, 2015.
# Kristjan SCHMIDT <kristjan.schmidt@googlemail.com>, 2011, 2012, 2015.
# Carmen Bianca BAKKER <carmen@carmenbianca.eu>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-books master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-books/issues\n"
"POT-Creation-Date: 2019-02-01 10:29+0000\n"
"PO-Revision-Date: 2019-02-22 14:03+0100\n"
"Last-Translator: Carmen Bianca BAKKER <carmen@carmenbianca.eu>\n"
"Language-Team: Esperanto <gnome-eo-list@gnome.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.2.1\n"
"X-DamnedLies-Scope: partial\n"
"X-Project-Style: gnome\n"

#: data/org.gnome.Books.appdata.xml.in:7 data/org.gnome.Books.desktop.in:3
#: src/application.js:104 src/overview.js:1030
msgid "Books"
msgstr "Libroj"

#: data/org.gnome.Books.appdata.xml.in:8
msgid "An e-book manager application for GNOME"
msgstr "Bitlibra mastrum-aplikaĵo por GNOME"

#: data/org.gnome.Books.appdata.xml.in:10
msgid ""
"A simple application to access and organize your e-books on GNOME. It is "
"meant to be a simple and elegant replacement for using a file manager to "
"deal with e-books."
msgstr ""
"Simpla aplikaĵo por aliri kaj organizi viajn bitlibrojn ene de GNOME. Ĝi "
"celas esti simpla kaj eleganta anstataŭigo de dosierfoliumilo, rilate al "
"dokumentojn."

#: data/org.gnome.Books.appdata.xml.in:15
msgid "It lets you:"
msgstr "Ĝi permesas al vi:"

#: data/org.gnome.Books.appdata.xml.in:17
msgid "View recent e-books"
msgstr "Rigardi freŝdatajn bitlibrojn"

#: data/org.gnome.Books.appdata.xml.in:18
msgid "Search through e-books"
msgstr "Serĉi tra dokumentoj"

#: data/org.gnome.Books.appdata.xml.in:19
msgid "View e-books (ePubs and comics) fullscreen"
msgstr "Montri bitlibrojn (ePub-ojn kaj komiksojn) tutekrane"

#: data/org.gnome.Books.appdata.xml.in:31
msgid "The GNOME Project"
msgstr "La GNOME-projekto"

#: data/org.gnome.Books.desktop.in:4
msgid "Access, manage and share books"
msgstr "Aliri, mastrumi kaj kunhavigi librojn"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: data/org.gnome.Books.desktop.in:7
msgid "org.gnome.Books"
msgstr "org.gnome.Books"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Books.desktop.in:15
msgid "Books;Comics;ePub;PDF;"
msgstr "Libroj;Komiksoj;ePub;PDF;"

#: data/org.gnome.books.gschema.xml:5
msgid "View as"
msgstr "Vidi kiel"

#: data/org.gnome.books.gschema.xml:6
msgid "View as type"
msgstr "Vidi kiel speco"

#: data/org.gnome.books.gschema.xml:10
msgid "Sort by"
msgstr "Ordigi laŭ"

#: data/org.gnome.books.gschema.xml:15
msgid "Window size"
msgstr "Fenestrogrando"

#: data/org.gnome.books.gschema.xml:16
msgid "Window size (width and height)."
msgstr "Fenestrogrando (larĝo kaj alto)."

#: data/org.gnome.books.gschema.xml:20
msgid "Window position"
msgstr "Pozicio de fenestro"

#: data/org.gnome.books.gschema.xml:21
msgid "Window position (x and y)."
msgstr "Pozicio de fenestro (x kaj y)."

#: data/org.gnome.books.gschema.xml:25
msgid "Window maximized"
msgstr "Fenestro maksimumigita"

#: data/org.gnome.books.gschema.xml:26
msgid "Window maximized state"
msgstr "Maksimumigita stato de fenestro"

#: data/org.gnome.books.gschema.xml:30
msgid "Night mode"
msgstr "Nokta reĝimo"

#: data/org.gnome.books.gschema.xml:31
msgid "Whether the application is in night mode."
msgstr "Ĉu la aplikaĵo estas en nokta reĝimo aŭ ne."

#: data/ui/books-app-menu.ui:6 data/ui/documents-app-menu.ui:6
#: src/preview.js:452
msgid "Night Mode"
msgstr "Nokta reĝimo"

#: data/ui/books-app-menu.ui:12 data/ui/documents-app-menu.ui:12
msgid "Keyboard Shortcuts"
msgstr "Klavokombinoj"

#: data/ui/books-app-menu.ui:16 data/ui/documents-app-menu.ui:16
msgid "Help"
msgstr "Helpo"

#: data/ui/books-app-menu.ui:20
msgid "About Books"
msgstr "Pri Libroj"

#: data/ui/documents-app-menu.ui:20
msgid "About Documents"
msgstr "Pri Dokumentoj"

#: data/ui/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Ĝenerala"

#: data/ui/help-overlay.ui:15
msgctxt "shortcut window"
msgid "Show help"
msgstr "Montri helpon"

#: data/ui/help-overlay.ui:22
msgctxt "shortcut window"
msgid "Quit"
msgstr "Ĉesi"

#: data/ui/help-overlay.ui:29
msgctxt "shortcut window"
msgid "Search"
msgstr "Serĉi"

#: data/ui/help-overlay.ui:36
msgctxt "shortcut window"
msgid "Print the current document"
msgstr "Presi la nunan dokumenton"

#: data/ui/help-overlay.ui:45
msgctxt "shortcut window"
msgid "Navigation"
msgstr "Navigado"

#: data/ui/help-overlay.ui:49 data/ui/help-overlay.ui:57
msgctxt "shortcut window"
msgid "Go back"
msgstr "Reen"

#: data/ui/help-overlay.ui:67
msgctxt "shortcut window"
msgid "Preview"
msgstr "Antaŭrigardo"

#: data/ui/help-overlay.ui:71
msgctxt "shortcut window"
msgid "Zoom in"
msgstr "Zomi"

#: data/ui/help-overlay.ui:78
msgctxt "shortcut window"
msgid "Zoom out"
msgstr "Malzomi"

#: data/ui/help-overlay.ui:85
msgctxt "shortcut window"
msgid "Bookmark the current page"
msgstr "Legosigni la nunan paĝon"

#: data/ui/help-overlay.ui:92
msgctxt "shortcut window"
msgid "Open places and bookmarks dialog"
msgstr "Malfermi lokan kaj legosignan dialogon"

#: data/ui/help-overlay.ui:99
msgctxt "shortcut window"
msgid "Copy selected text to clipboard"
msgstr "Kopii elektitan tekston al tondujo"

#: data/ui/help-overlay.ui:106
msgctxt "shortcut window"
msgid "Rotate counterclockwise"
msgstr "Turni maldekstrume"

#: data/ui/help-overlay.ui:113
msgctxt "shortcut window"
msgid "Rotate clockwise"
msgstr "Turni dekstrume"

#: data/ui/help-overlay.ui:120
msgctxt "shortcut window"
msgid "Next occurrence of the search string"
msgstr "Sekva okazo de la serĉĉeno"

#: data/ui/help-overlay.ui:127
msgctxt "shortcut window"
msgid "Previous occurrence of the search string"
msgstr "Antaŭa okazo de la serĉĉeno"

#: data/ui/help-overlay.ui:134
msgctxt "shortcut window"
msgid "Presentation mode"
msgstr "Prezenta reĝimo"

#: data/ui/help-overlay.ui:141
msgctxt "shortcut window"
msgid "Open action menu"
msgstr "Malfermi ago-menuon"

#: data/ui/help-overlay.ui:148
msgctxt "shortcut window"
msgid "Fullscreen"
msgstr "Tutekrana reĝimo"

#: data/ui/organize-collection-dialog.ui:40
msgid "Enter a name for your first collection"
msgstr "Enigu nomon por via unua kolekto"

#: data/ui/organize-collection-dialog.ui:56
#: data/ui/organize-collection-dialog.ui:113
msgid "New Collection…"
msgstr "Nova kolekto…"

#: data/ui/organize-collection-dialog.ui:73
#: data/ui/organize-collection-dialog.ui:122
msgid "Add"
msgstr "Aldoni"

#: data/ui/organize-collection-dialog.ui:163 data/ui/selection-toolbar.ui:88
#: src/overview.js:1034 src/search.js:121
msgid "Collections"
msgstr "Kolektoj"

#: data/ui/organize-collection-dialog.ui:167 src/overview.js:591
msgid "Cancel"
msgstr "Nuligi"

#: data/ui/organize-collection-dialog.ui:173
msgid "Done"
msgstr "Farita"

#: data/ui/preview-context-menu.ui:6
msgid "_Copy"
msgstr "_Kopii"

#. Translators: this is the Open action in a context menu
#: data/ui/preview-menu.ui:6 data/ui/selection-toolbar.ui:9
#: src/selections.js:976
msgid "Open"
msgstr "Malfermi"

#: data/ui/preview-menu.ui:10
msgid "Edit"
msgstr "Redakti"

#: data/ui/preview-menu.ui:15
msgid "Print…"
msgstr "Presi…"

#: data/ui/preview-menu.ui:21 src/preview.js:461
msgid "Fullscreen"
msgstr "Tutekrana reĝimo"

#: data/ui/preview-menu.ui:27
msgid "Present"
msgstr "Prezenti"

#: data/ui/preview-menu.ui:35
msgid "Zoom In"
msgstr "Zomi"

#: data/ui/preview-menu.ui:41
msgid "Zoom Out"
msgstr "Malzomi"

#: data/ui/preview-menu.ui:49
msgid "Rotate ↶"
msgstr "Turni ↶"

#: data/ui/preview-menu.ui:55
msgid "Rotate ↷"
msgstr "Turni ↷"

#: data/ui/preview-menu.ui:62 data/ui/selection-toolbar.ui:79
#: src/properties.js:61
msgid "Properties"
msgstr "Atributoj"

#: data/ui/selection-menu.ui:6
msgid "Select All"
msgstr "Elekti ĉion"

#: data/ui/selection-menu.ui:11
msgid "Select None"
msgstr "Elekti nenion"

#: data/ui/view-menu.ui:23
msgid "View items as a grid of icons"
msgstr "Vidigi erojn kiel bildsimbola kadro"

#: data/ui/view-menu.ui:40
msgid "View items as a list"
msgstr "Vidigi erojn kiel listo"

#: data/ui/view-menu.ui:73
msgid "Sort"
msgstr "Sortigi"

#: data/ui/view-menu.ui:84
msgid "Author"
msgstr "Aŭtoro"

#: data/ui/view-menu.ui:93
msgid "Date"
msgstr "Dato"

#: data/ui/view-menu.ui:102
msgid "Name"
msgstr "Nomo"

#: src/application.js:114
msgid "Show the version of the program"
msgstr "Montri la version de la programo"

#: src/documents.js:779
msgid "Failed to print document"
msgstr "Malsukcesis presi dokumenton"

#. Translators: this refers to local documents
#: src/documents.js:821 src/search.js:405
msgid "Local"
msgstr "Loka"

#: src/documents.js:878
msgid "Collection"
msgstr "Kolekto"

#: src/documents.js:1072
msgid ""
"You are using a preview of Books. Full viewing capabilities are coming soon!"
msgstr "Vi uzas antaŭrigardon de Libroj. Kompletaj vid-kapabloj baldaŭ venos!"

#. Translators: %s is the title of a document
#: src/documents.js:1098
#, javascript-format
msgid "Oops! Unable to load “%s”"
msgstr "Ups! Ne eblas ŝargi “%s”"

#: src/epubview.js:245
#, javascript-format
msgid "chapter %s of %s"
msgstr "ĉapitro %s de %s"

#: src/evinceview.js:524 src/lib/gd-places-bookmarks.c:656
msgid "Bookmarks"
msgstr "Legosignoj"

#: src/evinceview.js:532
msgid "Bookmark this page"
msgstr "Legosigni tiun ĉi paĝon"

#: src/lib/gd-nav-bar.c:242
#, c-format
msgid "Page %u of %u"
msgstr "Paĝo %u de %u"

#: src/lib/gd-pdf-loader.c:142
msgid "Unable to load the document"
msgstr "Ne eblis ŝargi la dokumenton"

#. Translators: %s is the number of the page, already formatted
#. * as a string, for example "Page 5".
#.
#: src/lib/gd-places-bookmarks.c:321
#, c-format
msgid "Page %s"
msgstr "Paĝo %s"

#: src/lib/gd-places-bookmarks.c:384
msgid "No bookmarks"
msgstr "Neniu legosigno"

#: src/lib/gd-places-bookmarks.c:392 src/lib/gd-places-links.c:257
msgid "Loading…"
msgstr "Ŝargante…"

#: src/lib/gd-places-links.c:342
msgid "No table of contents"
msgstr "Neniu enhavotabelo"

#: src/lib/gd-places-links.c:514
msgid "Contents"
msgstr "Enhavoj"

#: src/lib/gd-utils.c:328
msgid "translator-credits"
msgstr ""
"Tiffany ANTOPOLSKI\n"
"Kristjan SCHMIDT\n"
"Daniel PUENTES\n"
"Carmen Bianca BAKKER <carmen@carmenbianca.eu>"

#: src/lib/gd-utils.c:329
msgid "An e-books manager application"
msgstr "Bitlibra mastrum-aplikaĵo"

#: src/mainToolbar.js:101
msgctxt "menu button tooltip"
msgid "Menu"
msgstr "Menuo"

#: src/mainToolbar.js:111
msgctxt "toolbar button tooltip"
msgid "Search"
msgstr "Serĉi"

#: src/mainToolbar.js:120
msgid "Back"
msgstr "Reen"

#. Translators: only one item has been deleted and %s is its name
#: src/notifications.js:48
#, javascript-format
msgid "“%s” deleted"
msgstr "“%s” forigita"

#. Translators: one or more items might have been deleted, and %d
#. is the count
#: src/notifications.js:52
#, javascript-format
msgid "%d item deleted"
msgid_plural "%d items deleted"
msgstr[0] "%d ero estis forigita"
msgstr[1] "%d eroj estis forigitaj"

#: src/notifications.js:61 src/selections.js:383
msgid "Undo"
msgstr "Malfari"

#: src/notifications.js:159
#, javascript-format
msgid "Printing “%s”: %s"
msgstr "Presante “%s”: %s"

#: src/notifications.js:210
msgid "Your documents are being indexed"
msgstr "Viaj dokumentoj estas indeksataj"

#: src/notifications.js:211
msgid "Some documents might not be available during this process"
msgstr "Povas esti ke kelkaj dokumentoj ne estos disponeblaj dum procezo"

#: src/overview.js:287
msgid "No collections found"
msgstr "Neniu kolekto trovita"

#: src/overview.js:289
msgid "No books found"
msgstr "Neniu libro trovita"

#: src/overview.js:298
msgid "Try a different search"
msgstr "Provu alian serĉon"

#: src/overview.js:304
msgid "You can create collections from the Books view"
msgstr "Vi povas krei kolektojn el la Libroj vido"

#. Translators: this is the menu to change view settings
#: src/overview.js:537
msgid "View Menu"
msgstr "Vidi meuon"

#: src/overview.js:565
msgid "Click on items to select them"
msgstr "Klaki sur eroj por elekti ilin"

#: src/overview.js:567
#, javascript-format
msgid "%d selected"
msgid_plural "%d selected"
msgstr[0] "%d elektita"
msgstr[1] "%d elektitaj"

#: src/overview.js:648
msgid "Select Items"
msgstr "Elektitaj eroj"

#: src/overview.js:854
msgid "Yesterday"
msgstr "Hieraŭ"

#: src/overview.js:856
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "%d tago antaŭe"
msgstr[1] "%d tagoj antaŭe"

#: src/overview.js:860
msgid "Last week"
msgstr "Pasintsemajne"

#: src/overview.js:862
#, javascript-format
msgid "%d week ago"
msgid_plural "%d weeks ago"
msgstr[0] "%d semajno antaŭe"
msgstr[1] "%d semajnoj antaŭe"

#: src/overview.js:866
msgid "Last month"
msgstr "Pasintmonate"

#: src/overview.js:868
#, javascript-format
msgid "%d month ago"
msgid_plural "%d months ago"
msgstr[0] "%d monato antaŭe"
msgstr[1] "%d monatoj antaŭe"

#: src/overview.js:872
msgid "Last year"
msgstr "Pasintjare"

#: src/overview.js:874
#, javascript-format
msgid "%d year ago"
msgid_plural "%d years ago"
msgstr[0] "%d jaro antaŭe"
msgstr[1] "%d jaroj antaŭe"

#: src/password.js:42
msgid "Password Required"
msgstr "Necesas pasvorto"

#: src/password.js:45
msgid "_Unlock"
msgstr "_Malŝlosi"

#: src/password.js:61
#, javascript-format
msgid "Document %s is locked and requires a password to be opened."
msgstr "Dokumento %s estas ŝlosita, necesas pasvorton por malfermi ĝin."

#: src/password.js:75
msgid "_Password"
msgstr "_Pasvorto"

#. Translators: this is the Open action in a context menu
#: src/preview.js:444 src/selections.js:973
#, javascript-format
msgid "Open with %s"
msgstr "Malfermi per %s"

#: src/preview.js:769
msgid "Find Previous"
msgstr "Serĉi antaŭan"

#: src/preview.js:775
msgid "Find Next"
msgstr "Serĉi sekvan"

#. Title item
#. Translators: "Title" is the label next to the document title
#. in the properties dialog
#: src/properties.js:81
msgctxt "Document Title"
msgid "Title"
msgstr "Titolo"

#. Translators: "Author" is the label next to the document author
#. in the properties dialog
#: src/properties.js:90
msgctxt "Document Author"
msgid "Author"
msgstr "Aŭtoro"

#. Source item
#: src/properties.js:97
msgid "Source"
msgstr "Fonto"

#. Date Modified item
#: src/properties.js:103
msgid "Date Modified"
msgstr "Dato modifita"

#: src/properties.js:110
msgid "Date Created"
msgstr "Dato kreita"

#. Document type item
#. Translators: "Type" is the label next to the document type
#. (PDF, spreadsheet, ...) in the properties dialog
#: src/properties.js:119
msgctxt "Document Type"
msgid "Type"
msgstr "Speco"

#. Translators: "Type" refers to a search filter on the document type
#. (e-Books, Comics, ...)
#: src/search.js:116
msgctxt "Search Filter"
msgid "Type"
msgstr "Speco"

#. Translators: this refers to documents
#: src/search.js:119 src/search.js:225 src/search.js:399
msgid "All"
msgstr "Ĉiuj"

#: src/search.js:127
msgid "e-Books"
msgstr "Bitlibroj"

#: src/search.js:131
msgid "Comics"
msgstr "Komiksoj"

#. Translators: this is a verb that refers to "All", "Title", "Author",
#. and "Content" as in "Match All", "Match Title", "Match Author", and
#. "Match Content"
#: src/search.js:222
msgid "Match"
msgstr "Kongruo"

#. Translators: "Title" refers to "Match Title" when searching
#: src/search.js:228
msgctxt "Search Filter"
msgid "Title"
msgstr "Titolo"

#. Translators: "Author" refers to "Match Author" when searching
#: src/search.js:231
msgctxt "Search Filter"
msgid "Author"
msgstr "Aŭtoro"

#. Translators: "Content" refers to "Match Content" when searching
#: src/search.js:234
msgctxt "Search Filter"
msgid "Content"
msgstr "Enhavo"

#: src/search.js:395
msgid "Sources"
msgstr "Fontoj"

#: src/selections.js:356 src/selections.js:358
msgid "Rename…"
msgstr "Alinomi…"

#: src/selections.js:362 src/selections.js:364
msgid "Delete"
msgstr "Forigi"

#: src/selections.js:378
#, javascript-format
msgid "“%s” removed"
msgstr "“%s” forigita"

#: src/selections.js:775
msgid "Rename"
msgstr "Alinomi"

#. Translators: "Collections" refers to documents in this context
#: src/selections.js:781
msgctxt "Dialog Title"
msgid "Collections"
msgstr "Kolektoj"

#: src/trackerController.js:176
msgid "Unable to fetch the list of documents"
msgstr "Ne eblis venigi la liston de dokumentoj"

#~ msgid "Print e-books"
#~ msgstr "Presi bitlibrojn"

#~ msgid "Documents"
#~ msgstr "Dokumentoj"

#~ msgid "A document manager application for GNOME"
#~ msgstr "Dokumentmastrumilo por GNOME"

#~ msgid ""
#~ "A simple application to access, organize and share your documents on "
#~ "GNOME. It is meant to be a simple and elegant replacement for using a "
#~ "file manager to deal with documents. Seamless cloud integration is "
#~ "offered through GNOME Online Accounts."
#~ msgstr ""
#~ "Simpla aplikaĵo por aliri, organizi kaj kunhavigi viajn dokumentojn ene "
#~ "de GNOME. Ĝi celas esti simpla kaj eleganta anstataŭigo de "
#~ "dosieradministrilo, rilate al dokumentojn. Eblas flua integrado kun retaj "
#~ "kontoj pere de GNOME Retaj Kontoj."

#~ msgid "View recent local and online documents"
#~ msgstr "Vidi freŝdatajn lokajn kaj retajn dokumentojn"

#~ msgid "Access your Google, ownCloud or OneDrive content"
#~ msgstr "Aliri vian Google-, ownCloud- aŭ OneDrive-enhavon"

#~ msgid "Search through documents"
#~ msgstr "Serĉi tra dokumentoj"

#~ msgid "See new documents shared by friends"
#~ msgstr "Vidi novajn dokumentojn kunhavigataj de amikoj"

#~ msgid "View documents fullscreen"
#~ msgstr "Vidi dokumentojn tutekrane"

#~ msgid "Print documents"
#~ msgstr "Presi dokumentojn"

#~ msgid "Select favorites"
#~ msgstr "Elekti ŝatatajn"

#~ msgid "Allow opening full featured editor for non-trivial changes"
#~ msgstr "Permesi malfermon de kompleta redaktilo por netrivialaj ŝanĝoj"

#~ msgid "Access, manage and share documents"
#~ msgstr "Aliri, mastrumi kaj kunhavigi dokumentojn"

#~ msgid "org.gnome.Documents"
#~ msgstr "org.gnome.Documents"

#~ msgid "Docs;PDF;Document;"
#~ msgstr "Dok-oj;PDF;Dokumento;"

#~ msgid "About"
#~ msgstr "Pri"

#~ msgctxt "app menu"
#~ msgid "Quit"
#~ msgstr "Ĉesi"

#~ msgid "GNOME"
#~ msgstr "GNOME"

#~ msgid "Getting Started with Documents"
#~ msgstr "Komenci tuj kun Dokumentoj"

#~ msgid "Google Docs"
#~ msgstr "Google Docs"

#~ msgid "Google"
#~ msgstr "Google"

#~ msgid "Spreadsheet"
#~ msgstr "Kalkultabelo"

#~ msgid "Presentation"
#~ msgstr "Prezentaĵo"

#~ msgid "e-Book"
#~ msgstr "Bitlibro"

#~ msgid "Document"
#~ msgstr "Dokumento"

#~ msgid "ownCloud"
#~ msgstr "ownCloud"

#~ msgid "OneDrive"
#~ msgstr "OneDrive"

#~ msgid "Please check the network connection."
#~ msgstr "Bonvolu kontroli retkonekton."

#~ msgid "Please check the network proxy settings."
#~ msgstr "Bonvolu kontroli la retprokurilajn agordojn."

#~ msgid "Unable to sign in to the document service."
#~ msgstr "Ne eblis saluti en la dokumentservon."

#~ msgid "Unable to locate this document."
#~ msgstr "Ne eblis trovi la dokumenton."

#~ msgid "Hmm, something is fishy (%d)."
#~ msgstr "Hm, io malpravas (%d)."

#~ msgid ""
#~ "LibreOffice support is not available. Please contact your system "
#~ "administrator."
#~ msgstr ""
#~ "LibreOffice-subteno nedisponeblas. Bonvolu kontakti vian "
#~ "sistemadministranto."

#~ msgid "View"
#~ msgstr "Vido"

#~ msgid "A document manager application"
#~ msgstr "Dokumentmastrumila aplikaĵo"

#~ msgid "Fetching documents from %s"
#~ msgstr "Venigante dokumentojn de %s"

#~ msgid "Fetching documents from online accounts"
#~ msgstr "Venigante dokumentojn el retaj kontoj"

#~ msgid "No documents found"
#~ msgstr "Neniu dokumento trovita"

#~ msgid "You can create collections from the Documents view"
#~ msgstr "Vi povas krei kolektojn el la Dokumentoj vido"

#~ msgid ""
#~ "Documents from your <a href=\"system-settings\">Online Accounts</a> and "
#~ "<a href=\"file://%s\">Documents folder</a> will appear here."
#~ msgstr ""
#~ "Dokumentoj el via <a href=\"system-settings\">Retaj Kontoj</a> kaj <a "
#~ "href=\"file://%s\">Dokumentoj-dosiero</a> ĉi tie aperos."

#~ msgid "Running in presentation mode"
#~ msgstr "Rulante ĉe prezentaĵa reĝimo"

#~ msgid "Present On"
#~ msgstr "Prezenti je"

#~ msgid "Mirrored"
#~ msgstr "Spegulita"

#~ msgid "Primary"
#~ msgstr "Ĉefa"

#~ msgid "Off"
#~ msgstr "Malŝaltita"

#~ msgid "Secondary"
#~ msgstr "Duaranga"

#~ msgid "PDF Documents"
#~ msgstr "PDF-dokumentoj"

#~ msgid "Presentations"
#~ msgstr "Prezentaĵoj"

#~ msgid "Spreadsheets"
#~ msgstr "Kalkultabeloj"

#~ msgid "%s (%s)"
#~ msgstr "%s (%s)"

#~ msgid "Sharing Settings"
#~ msgstr "Kunhavigaj agordoj"

#~ msgid "Document permissions"
#~ msgstr "Dokumentaj permesoj"

#~ msgid "Change"
#~ msgstr "Ŝanĝi"

#~ msgid "Private"
#~ msgstr "Privata"

#~ msgid "Public"
#~ msgstr "Publika"

#~ msgid "Everyone can edit"
#~ msgstr "Ĉiuj povas redakti"

#~ msgid "Add people"
#~ msgstr "Aldoni personojn"

#~ msgid "Enter an email address"
#~ msgstr "Enigu retpoŝtadreson"

#~ msgid "Can edit"
#~ msgstr "Povas redakti"

#~ msgid "Can view"
#~ msgstr "Povas rigardi"

#~ msgid "Everyone can read"
#~ msgstr "Ĉiuj povas legi"

#~ msgid "Save"
#~ msgstr "Konservi"

#~ msgid "Owner"
#~ msgstr "Posedanto"

#~ msgid "You can ask %s for access"
#~ msgstr "Vi povas peti al %s pri aliro"

#~ msgid "The document was not updated"
#~ msgstr "Dokumenton ne ĝisdatiĝis"

#~ msgid "Untitled Document"
#~ msgstr "Sentitola dokumento"

#~| msgid "New and Recent"
#~ msgid "Recent"
#~ msgstr "Lasttempaj"

#~ msgid "LibreOffice is required to view this document"
#~ msgstr "LibreOffice estas bezonata por vidigi tiun ĉi dokumenton"

#~ msgid "Category"
#~ msgstr "Kategorio"

#~ msgid "Favorites"
#~ msgstr "Legosignoj"

#~ msgid "Shared with you"
#~ msgstr "Kunhavigita kun vi"

#~ msgid ""
#~ "You don't have any collections yet. Enter a new collection name above."
#~ msgstr "Vi ankoraŭ ne havas kolektojn. Enigu novan kolektonomon ĉi-supre."

#~ msgid "Create new collection"
#~ msgstr "Krei novan kolekton"

#~ msgid "Print"
#~ msgstr "Presi"

#~ msgid "Share"
#~ msgstr "Kunhavigi"

#~ msgid "Add to Collection"
#~ msgstr "Aldoni al koleto"

#~ msgid "You can add your online accounts in %s"
#~ msgstr "Vi povas aldoni viajn retajn kontojn en %s"

#~ msgid "Settings"
#~ msgstr "Agordoj"

#~ msgid "The active source filter"
#~ msgstr "La aktiva fontfiltrilo"

#~ msgid "The last active source filter"
#~ msgstr "La last aktiva fontfiltrilo"

#~ msgid "Grid"
#~ msgstr "Krado"

#~ msgid "List"
#~ msgstr "Listo"

#~ msgid "Unable to load \"%s\" for preview"
#~ msgstr "Ne eblis ŝargi na \"%s\" por antaŭmontro"

#~ msgid "Cannot find \"unoconv\", please check your LibreOffice installation"
#~ msgstr ""
#~ "Ne povas trovi na \"unoconv\", bonvolu kontroli vian instalaĵon de "
#~ "LibreOffice"

#~ msgid "Load %d more document"
#~ msgid_plural "Load %d more documents"
#~ msgstr[0] "Ŝargi %d pluan dokumenton"
#~ msgstr[1] "Ŝargi %d pluajn dokumentojn"

#~ msgid "(%d of %d)"
#~ msgstr "(%d de %d)"

#~ msgid "Organize"
#~ msgstr "Organizi"

#~ msgid "Remove from favorites"
#~ msgstr "Forigi de la legosignoj"

#~ msgid "Enable list view"
#~ msgstr "Enŝalti listvidon"
